-- check out this, to implement something similar to dwm/awesome 'tagging'
-- https://hackage.haskell.org/package/xmonad-contrib-0.15/docs/XMonad-Actions-CopyWindow.html



import XMonad
--import qualified XMonad.StackSet as W (shift, swapMaster)
import XMonad.StackSet (shift, swapMaster)
import XMonad.Actions.CycleWS (toggleWS, prevWS, nextWS, shiftToPrev, shiftToNext, toggleOrView)
import XMonad.Actions.FindEmptyWorkspace (viewEmptyWorkspace, tagToEmptyWorkspace)
import XMonad.Actions.GridSelect (GSConfig(..), goToSelected, bringSelected, colorRangeFromClassName, buildDefaultGSConfig)
import XMonad.Actions.UpdatePointer (updatePointer)
import XMonad.Util.EZConfig (additionalKeysP, removeKeysP)
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Layout
-- this won't work for firefox, because it doesn't automatically try to be
-- fullscreen, it waits for some signal that xmonad never gives & never will
--import XMonad.Hooks.EwmhDesktops



gsconfig' = (buildDefaultGSConfig (colorRangeFromClassName
        (0x0b,0x87,0x93)   --low  inactive bg
        (0x36,0x00,0x33)   --high inactive bg
        (0xfb,0x49,0x34)   --       active bg
        (0xfb,0xf1,0xc7)   --     inactive fg
        (0xfb,0xf1,0xc7))) --       active fg
    { gs_font        = "xft:Iosevka Slab Medium:size=15"
    , gs_cellwidth   = 200
    }


keybinds = [
      ("M-<Return>",    spawn "lxterminal")
    , ("M-r",           spawn "dmenu_run -i -p 'run '       -sb '#fb4934' -sf '#fbf1c7' -nb '#282828' -nf '#7c6f64' -fn 'Iosevka Slab Medium:size=15'")
    , ("M-p",           spawn "passmenu  -i -p 'password '  -sb '#fb4934' -sf '#fbf1c7' -nb '#282828' -nf '#7c6f64' -fn 'Iosevka Slab Medium:size=15'")
    , ("M-<Print>",     spawn "maim -s -b 2 -q ~/junk/$(date +%F-%H%M%S).png")
    -- can't bind to M-S-<Print> for some reason; it reads in like M-<Print>
    --, ("M-S-<Print>",   spawn "maim         ~/images/screenshots/mine/$(date +%F-%S).png")

    -- application shortcuts 
    , ("M-a",   spawn "geeqie")
    -- with phototonic, pass 'QT_QPA_PLATFORMTHEME' to change phototonic theme
    , ("M-s",   spawn "pcmanfm --no-desktop")
    , ("M-d",   spawn "lxterminal -e ranger")
    , ("M-e",   spawn "lxterminal -e nvim")
    -- this didn't work, because of the kill-keybind later; read more about
    -- xonsh instead
    --, ("M-p",   spawn "lxterminal -e ipython --quiet --no-banner --no-confirm-exit --nosep --colors='Linux'")
    , ("M-f", spawn "firefox")

    , ("M-S-<Return>", windows swapMaster)
    , ("M-`",          viewEmptyWorkspace)
    , ("M-<Tab>",      toggleWS)
    , ("M-g",          goToSelected  gsconfig')
    , ("M-S-g",        bringSelected gsconfig')
    -- volume controls
    , ("M-<F2>",   spawn "amixer -q set Master unmute 3%-")
    , ("M-<F3>",   spawn "amixer -q set Master unmute 3%+")
    , ("M-<F1>",   spawn "amixer -q set Master toggle")
    -- screen backlight controls
    -- small step, large step, set to min/max; values are float percentages
    , ("M-<F5>",                  spawn "xbacklight -dec 2   intel_backlight")
    , ("M-<F6>",                  spawn "xbacklight -inc 2   intel_backlight")
    , ("M-S-<F5>",                spawn "xbacklight -dec 5   intel_backlight")
    , ("M-S-<F6>",                spawn "xbacklight -inc 5   intel_backlight")
    , ("<XF86MonBrightnessDown>", spawn "xbacklight -set 0.1 intel_backlight")
    , ("<XF86MonBrightnessUp>",   spawn "xbacklight -set 100 intel_backlight")
    --, ("M-l",      spawn "xset dpms force off") --blanks the screen, doesn't actually lock it though
    ]
    ++
    -- views/toggles & shifts for all tags
    [("M-" ++ otherMasks ++ [key], action tag) | (tag, key) <- zip workspaces' "1234567890", (otherMasks, action) <- [("", toggleOrView), ("S-", windows . shift)] ]

killKeybinds = [
      ("M-q")   -- restarts xmonad; too close to ~, Tab, 1, 2
    , ("M-m")   -- focus on master window
    , ("M-m")   -- "resize viewed windows to the correct size" ?? 
    --, ("M-p")   -- launch dmenu
    , ("M-S-p") -- launch gmrun
    , ("M-?")   -- pops up an xmessage window w/ keybinds
    , ("M-S-/") -- ^^
    ]


workspaces' = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]


defaults = defaultConfig {
      borderWidth = 0
    , normalBorderColor  = "#282828"
    , focusedBorderColor = "#ccff00"
    , terminal = "lxterminal"
    , layoutHook  = smartBorders ( Tall 1 (1/50) (1/2) ||| Mirror (Tall 1 (1/50) (1/2)) ||| Full )
    , logHook     = updatePointer (0.2, 0.2) (0, 0)
    , manageHook  = composeAll [
        className =? "Conky" --> doIgnore ]
    , startupHook = spawn "conky -q"
    , workspaces  = workspaces'
    --use this to switch from 'alt' to 'win' as the global modifier
    --, modMask = mod4Mask
    } `additionalKeysP` keybinds `removeKeysP` killKeybinds

main = xmonad $ defaults

