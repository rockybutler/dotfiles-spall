"rockybutler@gmail.com
"last edit on 2019-03-21


filetype plugin indent on
    "enable filetype detection, allow loading file-specific plugins and indent
    "rules
syntax on
    "allow nvim to parse syntax & alter display based on syntax rulesets
set nomodeline
    "disable modeline support
set lazyredraw
    "postpone screen redraw until a command/macro/register finishes
set writebackup
    "create a temporary backup file during overwrite, just in case the write
    "fails; the backup is deleted when the write completes successfully
set clipboard=unnamed,unnamedplus
    "if no register is specified, use '+' (X CLIPBOARD) and '*' (X PRIMARY)
    "occasionally I overwrite the contents of both, by absent-mindedly selecting
    "text on webpages


call plug#begin()
Plug 'morhetz/gruvbox'  "colorscheme @ github.com/morhetz/gruvbox
call plug#end() 
    "use junegunn/vim-plugin to manage plugin functionality
    "to update plugins, issue ':PlugInstall'


set textwidth=80
    "max line length during insert mode
set nowrap
    "disable visual line wrapping / make long lines now extend off-screen
set scrolloff=3
    "always show # of lines above/below current line
set sidescrolloff=3
    "always show # of columns to the left/right of the cursor
set nostartofline
    "when changing lines, keep the same column position / don't go to 0

set formatoptions+=r
    "in insert mode, insert comment leader on next line after hitting <enter>
set selection=exclusive
    "in visual modes, the character under the cursor is not part of the
    "selection
set virtualedit=block
    "in 'visual block' mode, allow moving the cursor to positions that don't
    "contain characters
set nojoinspaces
    "only insert one space after a join operation (default is two spaces)
    
set tabstop=4
    "define the # of spaces a <tab> represents
set softtabstop=4
    "make editing operations treat a # of spaces as a <tab>
set expandtab
    "in insert mode, insert # of spaces equal to tabstop instead of actual tabs
set autoindent
    "automatically indent new lines
set shiftwidth=4
    "define # of spaces to use for autoindenting and '<', '>' commands
set shiftround
    "force '<' and '>' commands to round up/down to multiples of shiftwidth


set termguicolors
    "toggle support for 24-bit/true color colorschemes
let g:gruvbox_italic=1
    "enable italics; gruvbox disables them by default, so this line has to come
        "before loading that colorscheme
colorscheme gruvbox
set background=dark
    "use this colorscheme, with the dark background variant
set colorcolumn=81 
highlight colorcolumn ctermbg=8
    "set color value, and fill the background of column 81
set cursorline
    "fill the background of the current line (except in visual modes)

set guicursor=
    "disable changing the cursor shape
set laststatus=1
    "only show the status line during horizontal splits
set noerrorbells
    "disable audio and visual error bells for normal & ex modes
set shortmess=asI
    "disable or shorten most messages nvim emits; mostly this is to avoid the
    "'Press ENTER or...' prompt; check out 'help shortmess' for more info
"set list
    "turn on visible whitespace
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·
    "define which characters to use for visible whitespace

set ignorecase
    "ignore case for search terms; required for 'smartcase' to work
set smartcase
    "revert to case-sensitive search if the query starts with an uppercase
    "letter
set incsearch
    "update search results as each character is entered
set hlsearch
    "highlight (color background of) search results


"-- keybinds -------------------------------------------------------------------
"change command key to semicolon, so I don't have to hit <shift> all the time
noremap ; :
noremap : ;

"in visual modes, make the dot operator apply on a per-line basis
xnoremap . :normal .<cr>
"join two lines but don't insert any spaces; 'J' will insert one space
noremap K gJ

"a convenient way to clear search result highlighting
noremap <space> :nohlsearch<cr><space>
"a convenient way to cycle through tabs
noremap <tab>   :tabnext<cr>
noremap <S-tab> :tabprevious<cr>
"a convenient way to move between panes
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>

"disable some internal keybinds; can't use 'unmap' here, have to rely on setting
    "it to <nop>
"noremap <left>  <nop>
"noremap <down>  <nop>
"noremap <right> <nop>
"noremap <up>    <nop>

"change map-leader (for user-defined commands) to comma (default is '\')
let mapleader=','

"macros to create hr-like lines
"erase line, fill with -, and leave cursor @ column 4/replace mode
noremap <leader>8 0D80i-<esc>^2lr<space>lR
"erase line, fill with -, leave cursor on next line/normal mode
noremap <leader>9 0D80i-<esc>0j
"erase line, fill with - and today's date, leave cursor on next line/normal mode
noremap <leader>0 S--<esc>:r! date +\%Y-\%m-\%d<cr>o<esc>66i-<esc>kkJJo

"erase line & replace it with today's date
noremap <leader>d !!date +\%Y-\%m-\%d<cr>
"insert today's date in current line, prepended by one space
noremap <leader>s i<cr><cr><esc>k:.!date +\%Y-\%m-\%d<cr>kJgJ
noremap <leader>t A∎<esc>

"convenient way to change current filetype for unsaved buffers
noremap <leader>fm :set filetype=markdown<cr>
noremap <leader>fp :set filetype=python<cr>
noremap <leader>fj :set filetype=javascript<cr>

