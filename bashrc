#check for interactive session
[[ $- != *i* ]] && return

PS1='\W % '

#https://bbs.archlinux.org/viewtopic.php?id=191187
#unset PROMPT_COMMAND


#-------------------------------------------------------------------------------

export LANG='en_US.UTF-8'
export LOCALE='en_US.UTF-8'
export EDITOR='nvim'
export VISUAL='nvim'
export BROWSER='firefox'
export HISTCONTROL=ignoreboth:erasedups
export ALSACARD='PCH'
#what is this line for?
export TZ=':/etc/localtime'
#QT_STYLE_OVERRIDE='gtk'
#I think this is for phototonic?
export QT_QPA_PLATFORMTHEME='qt5ct'
export WORKON_HOME='~/.virtualenvs'
#firefox requires this, to keep it from mkdir'ing ~/Desktop
#XDG_DESKTOP_DIR='/home/rlb/'
export RANGER_LOAD_DEFAULT_RC=FALSE

alias ls='ls --color=auto --group-directories-first'
#alias f='free -hw'
#alias x='xargs -n 1 curl -s0 < urls'
alias grep='grep --color=auto'
#alias python='python -q'
#alias ipython='ipython --quiet --no-banner --no-confirm-exit --nosep --colors="Linux"'
alias p='ipython --quiet --no-banner --no-confirm-exit --nosep --colors="Linux"'
alias lst='tree -an --noreport --dirsfirst'
#alias vim='nvim'
alias blank='xset dpms force off'
alias b='acpi -V'

#-------------------------------------------------------------------------------

#python virutalenv script
source /usr/bin/virtualenvwrapper.sh

#enable command & filename completion for the following
complete -cf sudo
complete -cf man

